" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
  autocmd CocInstall coc-prettier
  "autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/autoload/plugged')

    " File explorer
    Plug 'scrooloose/nerdtree' | 
      \ Plug 'Xuyuanp/nerdtree-git-plugin'
    " Coc Plugin 
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    " Auto pairs for '(' '[' '{'
    Plug 'jiangmiao/auto-pairs'
    " Onedark theme
    Plug 'joshdick/onedark.vim'    
    " Font
    Plug 'ryanoasis/vim-devicons'
    " Terminal
    Plug 'kassio/neoterm'
    " Fuzzy Finder
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } } 
    Plug 'junegunn/fzf.vim'
    Plug 'BurntSushi/ripgrep'
    Plug 'sharkdp/fd'
" Telescope
    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-lua/telescope.nvim'
    " Parser
    Plug 'nvim-treesitter/nvim-treesitter'
    " Terminal
    Plug 'wincent/terminus'

" Code synthax
    " Better Syntax Support
    Plug 'sheerun/vim-polyglot'
    " Git plugin
    Plug 'tpope/vim-fugitive'
    " Terraform synthax
    Plug 'hashivim/vim-terraform'
    " Puppet synthax
    Plug 'rodjek/vim-puppet'
    " Autoformatter Prettier
    Plug 'neoclide/coc-prettier'
    " JSON synthax
    Plug 'elzr/vim-json'
    " Markdown synthax
    Plug 'tpope/vim-markdown'
    " Markdown table of content
    Plug 'mzlogin/vim-markdown-toc'
call plug#end()
