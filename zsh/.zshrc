# Lines configured by zsh-newuser-install
HISTFILE=~/.zsh_history
HISTSIZE=100000
SAVEHIST=100000
MS_BEFORE_SWITCH_TO_SEC=5000
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS
setopt autocd nomatch notify
unsetopt beep extendedglob
bindkey -e
# End of lines configured by zsh-newuser-install

# The following lines were added by compinstall
zstyle :compinstall filename '/home/didier/.zshrc'
autoload -Uz compinit
compinit
# End of lines added by compinstall

# Preferred editor for local and remote sessions
export PATH="$PATH:/opt/nvim-linux64/bin"
export VISUAL='/opt/nvim-linux64/bin/nvim'
export EDITOR="$VISUAL"
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR="$VISUAL"
else
  export EDITOR="$VISUAL"
fi

# Preferred editor for git
export GIT_EDITOR='/opt/nvim-linux64/bin/nvim'

# Statusbar scripts export
export PATH=/home/didier/.local/bin/statusbar:$PATH
export PATH=/home/didier/.local/bin:$PATH

# Aliases
alias nv="nvim"
alias cat="bat"
alias open="xdg-open"
alias terraform="tofu"

# Custom Prompt
autoload -U colors && colors    # Load colors
autoload -Uz vcs_info # enable vcs_info

zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' formats "%F{35}(%b %m)%f"
zstyle ':vcs_info:git*+set-message:*' hooks git-untracked

+vi-git-untracked() {
  if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] 
  then
    unstaged_modified=$(git status --porcelain | grep 'M ' | wc -l)
    added=$(git status --porcelain | grep '^A' | wc -l)
    unstaged_deleted=$(git status --porcelain | grep 'D ' | wc -l)
    untracked=$(git status --porcelain | grep '^??' | wc -l)
    hook_com[misc]="?$untracked +$added -$unstaged_deleted !$unstaged_modified"
  fi
}

# Replace home path with ~
function collapse_pwd {
    echo $(pwd | sed -e "s,^$HOME,~,")
}

# like git ps1 for openstack
__openstack_ps1()
{
    if [ -n "$OS_PROJECT_NAME" ] && [ -n "$OS_PASSWORD" ]; then
    (echo $OS_AUTH_URL | grep -q infomaniak.cloud) && local cluster=$(echo $OS_AUTH_URL | sed 's/.*api.\([^ ]*\).infomaniak.*/\1/')
    [ -z "$cluster" ] && cluster="openstack"
        printf -- "%s" " ($cluster|$OS_PROJECT_NAME)"
    fi
}

# Display current virtualenv
function virtualenv_info {
  [ $VIRTUAL_ENV ] && echo '('`basename $VIRTUAL_ENV`')'
}

# Timer for command execution
preexec() {
  timer=$(($(date +%s%0N)/1000000)) 
}

precmd() {
  elapsed=""
  if [ $timer ]; then
    now=$(($(date +%s%0N)/1000000))
    elapsed=$(($now-$timer))
    if [ $elapsed -gt $MS_BEFORE_SWITCH_TO_SEC ]; then
      elapsed="$(($elapsed/1000))s"
    else
      elapsed="⏳$(($elapsed))ms"
    fi
  fi
  unset timer
  vcs_info # load git
} # always load before displaying the prompt

setopt prompt_subst # enable command substitution in the prompt
NEWLINE=$'\n'
# First line of prompt
PROMPT='
┌%F{196}[%f%F{32}%n%f%F{7}@%f%F{208}%m%f%F{196}]%f%F{15}$(collapse_pwd)%f $vcs_info_msg_0_ $(__openstack_ps1)$(virtualenv_info) ${NEWLINE}└▪%F{99}$%f '
# Second line of prompt
RPROMPT='%(?. %F{10}${elapsed}%f %k.${elapsed}%f %k) %*'

# Fuzzy Finder
source /usr/share/fzf/completion.zsh
source /usr/share/fzf/key-bindings.zsh

# Git
fpath=(~/.zsh/zsh-completions $fpath)

# Bindkeys
bindkey "^[[3~" delete-char
bindkey ";5C" forward-word
bindkey ";5D" backward-word

# Load zprofile
source ~/.zprofile

# Start tmux when terminal starts
#case $- in *i*)
#    [ -z "$TMUX" ] && exec tmux
#esac

# End of custom configuration

autoload -U +X bashcompinit && bashcompinit

complete -o nospace -C /usr/local/bin/terraform terraform

# Load syntax highlighting; should be last.
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/.zsh/zsh-fzf/key-bindings.zsh
source ~/.zsh/zsh-fzf/completion.zsh
#source //usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
